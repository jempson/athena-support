package com.opdar.athena.support.controllers.support;

import com.opdar.athena.support.base.CommonInterceptor;
import com.opdar.athena.support.base.Constants;
import com.opdar.athena.support.base.ICacheManager;
import com.opdar.athena.support.base.Result;
import com.opdar.athena.support.entities.SupportUserEntity;
import com.opdar.athena.support.entities.UserEntity;
import com.opdar.athena.support.service.UserService;
import com.opdar.platform.annotations.Interceptor;
import com.opdar.platform.annotations.Request;
import com.opdar.platform.core.base.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Created by shiju on 2017/7/13.
 */
@Controller
@Interceptor(CommonInterceptor.class)
public class UserController {

    @Autowired
    UserService userService;

    @Request(value = "/user/update", format = Request.Format.JSON)
    public Result userUpdate(UserEntity userEntity) {
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        boolean ret = userService.update(userEntity);
        if(ret){
            return Result.valueOf(null);
        }
        return Result.valueOf(1,"更新用户信息出错");
    }


}
