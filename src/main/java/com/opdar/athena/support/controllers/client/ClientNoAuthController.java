package com.opdar.athena.support.controllers.client;

import com.opdar.athena.support.base.AppidInterceptor;
import com.opdar.athena.support.entities.UserEntity;
import com.opdar.athena.support.service.SupportUserService;
import com.opdar.athena.support.service.UserService;
import com.opdar.platform.annotations.Interceptor;
import com.opdar.platform.annotations.Request;
import com.opdar.platform.core.base.Context;
import com.opdar.platform.core.session.ISessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import java.util.UUID;

/**
 * Created by shiju on 2017/7/13.
 */
@Controller
public class ClientNoAuthController {

    @Autowired
    ISessionManager<Object> sessionManager;
    @Autowired
    SupportUserService supportUserService;
    @Autowired
    UserService userService;
    /**
     * 测试
     * @param userName
     * @param userPwd
     * @return
     */
    @Request(value = "/client/register", format = Request.Format.VIEW)
    public String register(String userName, String userPwd,String appId){
        if(!StringUtils.isEmpty(userName) && !StringUtils.isEmpty(userPwd)){
            boolean user = userService.regist(userName, userPwd);
            if(user){
                String token = UUID.randomUUID().toString();
                Context.getRequest().getSession().setAttribute("token",token);
                return "redirect:login";
            }
        }
        return "client/register";
    }

    @Interceptor(AppidInterceptor.class)
    @Request(value = "/client/login", format = Request.Format.VIEW)
    public String login(String userName, String userPwd,String appId,String utmSource){
        Cookie cookie = new Cookie("CLIENT_APP_ID",appId);
        Context.getResponse().addCookie(cookie);
        if(!StringUtils.isEmpty(userName) && !StringUtils.isEmpty(userPwd)){
            UserEntity user = userService.login(userName, userPwd);
            if(user != null){
                user.setAppId(appId);
                String token = UUID.randomUUID().toString();
                if(!StringUtils.isEmpty(utmSource))
                    user.setUtmSource(utmSource);
                Context.getRequest().getSession().setAttribute("token",token);
                sessionManager.set(token,user,24*60*60*1000);
                return "redirect:index";
            }
        }
        return "client/login";
    }

    @Request(value = "/client/anonymous", format = Request.Format.VIEW)
    public String anonymous(String appId,String utmSource){
        Cookie[] cookies = Context.getRequest().getCookies();
        String userName = null;
        for(Cookie cookie:cookies){
            if(cookie.getName().equals("AUSERNAME")){
                userName = cookie.getValue();
            }
        }
        UserEntity user = userService.anonymous(userName);
        if(user != null){
            user.setAppId(appId);
            if(!StringUtils.isEmpty(utmSource))
                user.setUtmSource(utmSource);
            Context.getResponse().addCookie(new Cookie("AUSERNAME",user.getUserName()));
            String token = UUID.randomUUID().toString();
            Context.getRequest().getSession().setAttribute("token",token);
            sessionManager.set(token,user,24*60*60*1000);
            return "redirect:index";
        }
        return "client/login";
    }

}
